import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AuthenticationGuard, MsAdalAngular6Module } from 'microsoft-adal-angular6';
import { HttpClientModule} from '@angular/common/http';

// *******************************************************************************
// NgBootstrap

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


// *******************************************************************************
// Libs

import { SweetAlert2Module } from '@toverux/ngx-sweetalert2';
import { ToastrModule } from 'ngx-toastr';
import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
import { ContextMenuModule } from 'ngx-contextmenu';
import { TourNgBootstrapModule } from 'ngx-tour-ng-bootstrap';
import { AgmCoreModule } from '@agm/core';
import { BlockUIModule } from 'ng-block-ui';
import { CalendarModule } from 'angular-calendar';

// *******************************************************************************
// App

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppService } from './app.service';
import { LayoutModule } from './layout/layout.module';
import { ThemeSettingsModule } from '../vendor/libs/theme-settings/theme-settings.module';
// servicio a API
import { UsersService } from './users.service';


// *******************************************************************************
//

@NgModule({
  declarations: [
    AppComponent
  ],

  imports: [
    HttpClientModule,
    MsAdalAngular6Module.forRoot({
      tenant: 'de8e3e38-3cb9-4e58-9a81-f6204bbe6833',
      clientId: '6aa0ea49-ae83-44a0-9f78-0302a87053fc',
      redirectUri: 'http://bpmdev.strategying.com/aad/',
      endpoints: {
        'https://graph.microsoft.com': '6aa0ea49-ae83-44a0-9f78-0302a87053fc'
      },
      navigateToLoginRequestUrl: false,
      cacheLocation: 'localStorage',
    }),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgbModule.forRoot(),

    // App
    AppRoutingModule,
    LayoutModule,
    ThemeSettingsModule,

    // Libs
    SweetAlert2Module.forRoot({
      buttonsStyling: false,
      confirmButtonClass: 'btn btn-lg btn-primary',
      cancelButtonClass: 'btn btn-lg btn-default'
    }),
    ToastrModule.forRoot(),
    ConfirmationPopoverModule.forRoot({
      cancelButtonType: 'default btn-sm',
      confirmButtonType: 'primary btn-sm'
    }),
    ContextMenuModule.forRoot(),
    TourNgBootstrapModule.forRoot(),
    AgmCoreModule.forRoot({
      /* NOTE: When using Google Maps on your own site you MUST get your own API key:
               https://developers.google.com/maps/documentation/javascript/get-api-key
               After you got the key paste it in the URL below. */
      apiKey: 'AIzaSyCHtdj4L66c05v1UZm-nte1FzUEAN6GKBI'
    }),
    BlockUIModule.forRoot(),
    CalendarModule.forRoot()
  ],

  providers: [
    Title,
    AppService,
    AuthenticationGuard,
    UsersService
  ],

  bootstrap: [
    AppComponent
  ]
})
export class AppModule {}
