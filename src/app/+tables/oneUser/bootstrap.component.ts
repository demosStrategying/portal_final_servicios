import { Component } from '@angular/core';
import { AppService } from '../../app.service';
import { UsersService } from '../../users.service';

@Component({
  selector: 'tables-bootstrap', // tslint:disable-line
  templateUrl: './bootstrap.component.html'
})
export class Bootstrap2Component {
  people$: any;
  todos$: any;
  hidded = false;

  constructor(
    private peopleService: UsersService,
    private appService: AppService
  ) {
    //  constructor(private appService: AppService) {
    this.appService.pageTitle = 'Bootstrap - Tables';
  }
  removeUsers() {
    this.people$ = '';
    this.toggleBtn();
  }
  toggleBtn() {
    if (this.hidded === true) {
      this.hidded = false;
      console.log(this.hidded);
    } else if (this.hidded === false) {
      this.hidded = true;
      console.log(this.hidded);
    }
  }

   fetchUnRegistro() {
     this.todos$ = this.peopleService.fetchUnRegistro();

   }
}
