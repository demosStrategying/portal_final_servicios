import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BootstrapComponent } from './bootstrap/bootstrap.component';
import { NgxDatatableComponent } from './ngx-datatable/ngx-datatable.component';
import { Ng2SmartTableComponent } from './ng2-smart-table/ng2-smart-table.component';
import { AuthenticationGuard } from 'microsoft-adal-angular6';
import { Bootstrap2Component } from './oneUser/bootstrap.component';


// *******************************************************************************
//

@NgModule({
  imports: [RouterModule.forChild([
    { path: 'bootstrap', component: BootstrapComponent },
    { path: 'bootstrap2', component: Bootstrap2Component },
    { path: 'dinamica', component: NgxDatatableComponent, /*canActivate: [AuthenticationGuard] */},
    { path: 'dinamica-accion', component: Ng2SmartTableComponent, /*canActivate: [AuthenticationGuard] */},
  ])],
  exports: [RouterModule]
})
export class TablesRoutingModule { }
