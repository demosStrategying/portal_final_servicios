import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
@Injectable()
export class UsersService {


  constructor(private http: HttpClient){

    }
    fetchPeople() {
      // works
      return this.http.get('https://jsonplaceholder.typicode.com/users/');
    }
    fetchUnRegistro() {
      // return an object
      return this.http.get('https://jsonplaceholder.typicode.com/users/1');
    }
}
