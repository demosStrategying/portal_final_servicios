import { Component, Input, HostBinding } from '@angular/core';
import { AppService } from '../../app.service';
import { LayoutService } from '../../layout/layout.service';
import {  MsAdalAngular6Service } from 'microsoft-adal-angular6';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';


@Component({
  selector: 'app-layout-navbar',
  templateUrl: './layout-navbar.component.html',
  styles: [':host { display: block; }'],
  styleUrls: ['./layout-navbar.component.css']
})
export class LayoutNavbarComponent {
  isExpanded = false;
  isRTL: boolean;
  myinfo: string;
loggedUser = '';

  @Input() sidenavToggle = true;

  @HostBinding('class.layout-navbar') private hostClassMain = true;

  constructor(private route: ActivatedRoute,
    private router: Router, private appService: AppService, private layoutService: LayoutService, private adalSvc: MsAdalAngular6Service) {
    if (!this.adalSvc.isAuthenticated) {
      // this.adalSvc.login();
      //  window.location.href = 'http://localhost:3624/pages/authentication/login-v3';
      //  this.router.navigate(['/pages/authentication/login-v3']);

     } else {
       // console.log('autenticado como : '  + this.adalSvc.LoggedInUserName);
     }
         //console.log(this.adalSvc.userInfo);
// tslint:disable-next-line: prefer-const
        let token = this.adalSvc.acquireToken('https://graph.microsoft.com').subscribe((token: string) => {
           console.log(token);

           // mirar estooooo
           // mirar estooooo
           // mirar estooooo
           // mirar estooooo
            this.loggedUser = this.adalSvc.LoggedInUserName;
            this.myinfo = this.adalSvc.accessToken;
           // mirar estooooo
         });

    this.isRTL = appService.isRTL;
  }

  currentBg() {
    // return `bg-${this.appService.layoutNavbarBg}`;
    return `bg-str`;
  }

  toggleSidenav() {
    this.layoutService.toggleCollapsed();
  }

  desconexion() {
    this.adalSvc.logout();
  }
}
