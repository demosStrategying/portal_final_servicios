import { Component } from '@angular/core';
import { IOption } from 'ng-select';

@Component({
  selector: 'cui-ng-select', // tslint:disable-line
  templateUrl: './cui-ng-select.component.html',
  styleUrls: ['../../../vendor/libs/ng-select/ng-select.scss']
})
export class CuiNgSelectComponent {
  disabled = false;
  selectOptions: Array<IOption> = [
    { label: 'Alaska', value: 'AK' },
    { label: 'Hawaii', value: 'HI' },
    { label: 'California', value: 'CA' },
    { label: 'Nevada', value: 'NV' },
    { label: 'Oregon', value: 'OR' },
    { label: 'Washington', value: 'WA' },
    { label: 'Arizona', value: 'AZ' },
    { label: 'Colorado', value: 'CO' },
    { label: 'Idaho', value: 'ID' },
    { label: 'Montana', value: 'MT' },
    { label: 'Nebraska', value: 'NE' },
    { label: 'New Mexico', value: 'NM' },
    { label: 'North Dakota', value: 'ND' },
    { label: 'Utah', value: 'UT' },
    { label: 'Wyoming', value: 'WY' },
    { label: 'Alabama', value: 'AL' },
    { label: 'Arkansas', value: 'AR' },
    { label: 'Illinois', value: 'IL' },
    { label: 'Iowa', value: 'IA' },
    { label: 'Kansas', value: 'KS' },
    { label: 'Kentucky', value: 'KY' },
    { label: 'Louisiana', value: 'LA' },
    { label: 'Minnesota', value: 'MN' },
    { label: 'Mississippi', value: 'MS' },
    { label: 'Missouri', value: 'MO' },
    { label: 'Oklahoma', value: 'OK' },
    { label: 'South Dakota', value: 'SD' },
    { label: 'Texas', value: 'TX' },
    { label: 'Tennessee', value: 'TN' },
    { label: 'Wisconsin', value: 'WI' },
    { label: 'Connecticut', value: 'CT' },
    { label: 'Delaware', value: 'DE' },
    { label: 'Florida', value: 'FL' },
    { label: 'Georgia', value: 'GA' },
    { label: 'Indiana', value: 'IN' },
    { label: 'Maine', value: 'ME' },
    { label: 'Maryland', value: 'MD' },
    { label: 'Massachusetts', value: 'MA' },
    { label: 'Michigan', value: 'MI' },
    { label: 'New Hampshire', value: 'NH' },
    { label: 'New Jersey', value: 'NJ' },
    { label: 'New York', value: 'NY' },
    { label: 'North Carolina', value: 'NC' },
    { label: 'Ohio', value: 'OH' },
    { label: 'Pennsylvania', value: 'PA' },
    { label: 'Rhode Island', value: 'RI' },
    { label: 'South Carolina', value: 'SC' },
    { label: 'Vermont', value: 'VT' },
    { label: 'Virginia', value: 'VA' },
    { label: 'West Virginia', value: 'WV' }
  ];

  singleSelectValue: IOption;
  multipleSelectValue: Array<IOption> = [];
}
